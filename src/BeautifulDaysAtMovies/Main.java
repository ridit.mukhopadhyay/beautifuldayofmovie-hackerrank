package BeautifulDaysAtMovies;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		System.out.println("Enter lower limit");
		int lowerLimit = in.nextInt();
		System.out.println("Enter upper limit");
		int upperLimit = in.nextInt();
		System.out.println("Enter the divisor");
		int k = in.nextInt();
		System.out.println("the answer is " + Count(lowerLimit, upperLimit, k));

	}
	public static int Count(int lowerLimit,int upperLimit,int k) {
		int count = 0;
		for(int i = lowerLimit;i<=upperLimit;i++) {
			String numStrForm = Integer.toString(i);
			StringBuffer reversebuff = new StringBuffer(numStrForm);
			String reverse = reversebuff.reverse().toString();
			int iReverse = Integer.parseInt(reverse);
			if((i - iReverse)% k == 0) {
				count++;
			}
		}
		return count;
	}

}
